package ru.mihaylov.onlinetv

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.mihaylov.onlinetv.model.data.repository.DefaultMoviesRepository
import ru.mihaylov.onlinetv.model.data.repository.MoviesRepository
import ru.mihaylov.onlinetv.ui.main.MoviesViewModel

class ViewModelFactory(
    private val moviesRepository: MoviesRepository
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        when {
            modelClass.isAssignableFrom(MoviesViewModel::class.java) ->
                MoviesViewModel(moviesRepository)
            else ->
                throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
        } as T


}