package ru.mihaylov.words.di

import android.content.Context
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.mihaylov.onlinetv.model.data.server.MovieApi
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class ApiModule(private val context: Context) {

    @Provides
    @Singleton
    fun httpClient(): OkHttpClient {
        val cache = Cache(File(context.cacheDir, "http-cache"), CACHE_SIZE)
        return OkHttpClient.Builder()
            .addNetworkInterceptor(provideCacheInterceptor())
            .cache(cache)
            .build()
    }

    private fun provideCacheInterceptor(): Interceptor {
        return Interceptor { chain ->
            val response: Response = chain.proceed(chain.request())
            val cacheControl = CacheControl.Builder()
                .maxAge(1, TimeUnit.DAYS)
                .build()
            response.newBuilder()
                .header("Cache-Control", cacheControl.toString())
                .build()
        }
    }

    @Provides
    @Singleton
    fun movieApi(httpClient: OkHttpClient): MovieApi {
        val gson = GsonBuilder().create()
        val retrofitConfig = Retrofit.Builder()
            .client(httpClient)
            .baseUrl(API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson)).build()

        return retrofitConfig.create(MovieApi::class.java)
    }

    companion object {

        const val CACHE_SIZE = 10 * 1024L
        const val API_BASE_URL = "https://raw.githubusercontent.com/ar2code/apitest/master/"

    }

}