package ru.mihaylov.onlinetv.entity

data class Movie(
    val id: Long,
    val posterUrl: String,
    val year: Long
)
