package ru.mihaylov.onlinetv.model.data.repository

import ru.mihaylov.onlinetv.entity.Filter
import ru.mihaylov.onlinetv.entity.Movie
import ru.mihaylov.onlinetv.model.data.server.MovieApi


class DefaultMoviesRepository(private val movieApi: MovieApi): MoviesRepository {

    override suspend fun getMoviesList(filter: Filter): List<Movie> {
        return movieApi.getMovieList()
            .map { it.toEntity() }
            .filter {
                !filter.isOnly2020Year || it.year == 2020L
            }
    }
}