package ru.mihaylov.onlinetv.model.data.repository

import ru.mihaylov.onlinetv.entity.Filter
import ru.mihaylov.onlinetv.entity.Movie

interface MoviesRepository {

    suspend fun getMoviesList(filter: Filter) : List<Movie>

}