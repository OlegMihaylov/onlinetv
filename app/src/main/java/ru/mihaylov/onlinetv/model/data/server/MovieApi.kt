package ru.mihaylov.onlinetv.model.data.server

import retrofit2.http.GET

interface MovieApi {

    @GET("movies.json")
    suspend fun getMovieList() : List<RestMovie>

}