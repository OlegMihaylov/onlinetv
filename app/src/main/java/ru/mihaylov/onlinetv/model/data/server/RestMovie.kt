package ru.mihaylov.onlinetv.model.data.server

import ru.mihaylov.onlinetv.entity.Movie

data class RestMovie(
    val id: Long,
    val poster: String,
    val year: Long
) {
    fun toEntity(): Movie {
        return Movie(id, poster, year)
    }

}
