package ru.mihaylov.onlinetv.ui.common

import android.widget.ImageView
import com.bumptech.glide.Glide

/**
 * Отобразить изображение [url] в текущем ImageView
 */
fun ImageView.loadByUrl(url: String?) {
    Glide
        .with(this.context)
        .load(url)
        .centerCrop()
        .into(this)
}