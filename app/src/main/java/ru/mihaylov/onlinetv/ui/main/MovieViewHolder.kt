package ru.mihaylov.onlinetv.ui.main

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.movie_item.view.*
import ru.mihaylov.onlinetv.entity.Movie
import ru.mihaylov.onlinetv.ui.common.loadByUrl

class MovieViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val movieImage = view.movie_image

    fun bind(movie: Movie) {
        movieImage.loadByUrl(movie.posterUrl)
    }

}