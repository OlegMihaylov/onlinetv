package ru.mihaylov.onlinetv.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.main_fragment.*
import ru.mihaylov.onlinetv.App
import ru.mihaylov.onlinetv.R
import ru.mihaylov.onlinetv.ui.common.SpacingItemDecoration
import javax.inject.Inject

class MoviesFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<MoviesViewModel> { viewModelFactory }
    private val moviesAdapter = MoviesAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.appComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initializeRecyclerView()
        initializeDataObservers()
        filter_state.setOnCheckedChangeListener { _, isChecked -> viewModel.changeFilterState(isChecked) }
    }

    private fun initializeRecyclerView() {
        val spanCount = resources.getInteger(R.integer.span_count)
        val gridLayoutManager = GridLayoutManager(context, spanCount)
        val itemDecoration = SpacingItemDecoration(spanCount, 8)
        movie_list.adapter = moviesAdapter
        movie_list.layoutManager = gridLayoutManager
        movie_list.addItemDecoration(itemDecoration)
        movie_list.setHasFixedSize(true)
    }

    private fun initializeDataObservers() {
        viewModel.isLoading.observe(viewLifecycleOwner, Observer {
            loader_block.visibility = if (it) View.VISIBLE else View.GONE
        })
        viewModel.isError.observe(viewLifecycleOwner, Observer {
            load_error_block.visibility = if (it) View.VISIBLE else View.GONE
        })
        viewModel.movieList.observe(viewLifecycleOwner, Observer {
            moviesAdapter.submitList(viewModel.movieList.value)
        })
    }

    companion object {
        fun newInstance() = MoviesFragment()
    }

}