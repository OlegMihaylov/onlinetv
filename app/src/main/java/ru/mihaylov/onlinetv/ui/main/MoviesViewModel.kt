package ru.mihaylov.onlinetv.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import ru.mihaylov.onlinetv.entity.Filter
import ru.mihaylov.onlinetv.entity.Movie
import ru.mihaylov.onlinetv.model.data.repository.MoviesRepository

class MoviesViewModel(
    private val moviesRepository: MoviesRepository
) : ViewModel() {

    val movieList = MutableLiveData<List<Movie>>()
    val isLoading = MutableLiveData(false)
    val isError = MutableLiveData(false)
    private var filter = Filter()

    init {
        loadMovieList()
    }

    private fun loadMovieList() {
        isLoading.value = true
        viewModelScope.launch {
            try {
                movieList.value = moviesRepository.getMoviesList(filter)
                isLoading.value = false
            } catch (ignore: Exception) {
                isLoading.value = false
                isError.value = true
            }
        }
    }

    fun changeFilterState(value: Boolean) {
        filter.isOnly2020Year = value
        viewModelScope.launch {
            try {
                movieList.value = moviesRepository.getMoviesList(filter)
                isError.value = false
            } catch (ignore: Exception) {
                isError.value = true
            }
        }
    }

}